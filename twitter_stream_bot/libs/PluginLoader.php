<?php
namespace stream_bot\libs;

class PluginLoader{
	
	private $plugins_dir;
	private $plugins = [];
	
	public function __construct($plugins_dir){
		$this->plugins_dir = $plugins_dir;
	}
	
	public function load($oauth){
		echo "[plugin load start]\n";
		$plugins = [];
		$plugin_names = scandir($this->plugins_dir);
		foreach($plugin_names as $plugin_name){
			$file_dir = $this->plugins_dir.$plugin_name;
			if(is_file($file_dir) && preg_match('/^.*\.php$/',$plugin_name)==1){
				try{
					require_once($file_dir);
					$plugin_class = '\\'.str_replace('.php','',$plugin_name);
					$plugin = new $plugin_class($oauth);
					if( isset($plugin) && $plugin instanceof \stream_bot\libs\Plugin ){
						$plugins[] = $plugin;
						echo '[plugin]  ',$plugin->name(),' : ',$plugin->version()," loaded\n";
					}
				}catch(Exception $e){
					echo $e->getMessage()."\n";
				}
			}
		}
		$this->plugins = $plugins;
	}
	
	public function getPlugins(){
		return $this->plugins;
	}
}