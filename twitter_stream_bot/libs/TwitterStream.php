<?php
namespace stream_bot\libs;

class TwitterStream{

	const host		= 'userstream.twitter.com';
	const full_url	= 'https://userstream.twitter.com/1.1/user.json';
	
	private $oauth;
	
	private static $connection_retry	= 8;
	private static $retry_interval		= 5;
	
	private $retry = 0;
	
	private $callbacks = [];
	
	private $debug = false;
	
	public function __construct(\OAuth $oauth,$callbacks = []){
		$this->oauth = $oauth;
		$this->callbacks = $callbacks;
	}
	
	public function enableDebug(){
		$this->debug = true;
	}
	
	public function disableDebug(){
		$this->debug = false;
	}
	
	private function getRequestHeader(){
		return $this->oauth->getRequestHeader('GET',self::full_url);
	}
	
	private function getRequest(){
		return 	'GET '.self::full_url.' HTTP/1.1'."\r\n"
				. 'Host: '.self::host."\r\n"
				. 'Authorization: '.$this->getRequestHeader(). "\r\n"
				. "\r\n";
	}
	

	
	public function start(){
		while(1){
			$this->conect();
			if( !$this->onRunable() || $this->retry > static::$connection_retry)break;
			$this->retry++;
			sleep(static::$retry_interval*$this->retry*$this->retry);
		}
	}
	
	private function conect(){
		if(($fp = fsockopen('tls://'.self::host, 443,$error_no,$error_str))===false){
			$response	= ['code' => $error_no,'message'=>'open error: '.$error_str];
			$header		= [];
			$this->onError($response,$header);
			return;
		}
		if(fwrite($fp,$this->getRequest())===false){
			$response	= ['code' => -1,'message'=>'request write error'];
			$header		= [];
			$this->onError($response,$header);
			return;
		}
		stream_set_timeout($fp, 30);
		
		//read header
		$response	= ['code' => -1,'message'=>''];
		$header		= [];
		while (($in = fgets($fp))!=false && $this->onRunable()) {
			if($this->debug)echo $in,"\n";
			if($in==="\r\n")break;
			elseif(preg_match("/^(.+): (.+)\r\n$/",$in,$matches)){
				$header[$matches[1]] = $matches[2];
			}elseif(preg_match("/^HTTP\/1\.1 (.+) (.*)\r\n$/",$in,$matches)){
				$response['code'] = $matches[1];
				$response['message'] = $matches[2];
			}
		}
		
		if($response['code'] !== '200'){
			$this->onError($response,$header);
			return;
		}
		
		$this->onConection($response,$header);
		
		$this->retry = 0;

		$in = '';
		while (($line = fgets($fp)) != false && $this->onRunable()) {
			$in .= $line;
			if($this->debug)echo "---------------------start---------------------\n",$line,"---------------------end---------------------\n";
			if(preg_match("/(.+)\r\n(.*)\r\n\r\n/",$in,$matches)){
				if( strlen($matches[2])>0 ){
					$json = json_decode($matches[2]);
					if($json){
						if( isset($json->disconnect)){
							$this->onDisconection($json);
						}else{
							$this->onJson($json);
						}
					}
				}
				$in='';
			}
		}
		fclose($fp);
		$this->onShutdown();
	}
	
	
	private function onShutdown(){
		if( isset($this->callbacks["onShutdown"]) ){
			call_user_func($this->callbacks["onShutdown"]);
		}
	}
	
	private function onConection($response,$header){
		if( isset($this->callbacks["onConection"]) ){
			call_user_func($this->callbacks["onConection"],$response,$header);
		}
	}
	
	private function onDisconection($json){
		if( isset($this->callbacks["onDisconection"]) ){
			call_user_func($this->callbacks["onDisconection"],$json);
		}
	}
	private function onRunable(){
		if( isset($this->callbacks["onRunable"]) ){
			return call_user_func($this->callbacks["onRunable"]);
		}else{
			return true;
		}
	}
	private function onJson($json){
		if( isset($this->callbacks["onJson"]) ){
			call_user_func($this->callbacks["onJson"],$json);
		}
	}
	private function onError($response,$header){
		if( isset($this->callbacks["onError"]) ){
			call_user_func($this->callbacks["onError"],$response,$header);
		}
	}
}
