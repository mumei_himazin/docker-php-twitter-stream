<?php
namespace stream_bot\libs;

class Plugin{
	
	private $oauthParameters;
	
	public function __construct($oauthParameters){
		$this->oauthParameters = $oauthParameters;
	}
	
	public function name(){
		return 'plugin name';
	}
	public function version(){
		return '1.0';
	}
	
	function onSender($json){}
	
	function onStatus($json){}
	
	function onDirectMessage($json){}
	
	function onDelete($json){}
	
	function onLimit($json){}
	
	function onWarning($json){}
	
	function onScrubGeo($json){}
	
	function onFriends($json){}
	
	function onFavorite($json){}
	
	function onUnfavorite($json){}
	
	function onFollow($json){}
	
	function onUnfollow($json){}
	
	function onUserUpdate($json){}
	
	function onBlock($json){}
	
	function onUnblock($json){}
	
	function onListMemberAdded($json){}
	
	function onListMemberRemoved($json){}
	
	function onListUserSubscribed($json){}
	
	function onListUserUnsubscribed($json){}
	
	function onListCreated($json){}
	
	function onListUpdated($json){}
	
	function onListDestroyed($json){}
	
	/**
	 *	https://dev.twitter.com/docs/api/1.1/post/statuses/update
	 *	連想配列で上記URLのParametersをキーにして
	 */
	function statusUpdate($parameters){
		if(!isset($parameters['status']))return false;
		$response = $this->post('https://api.twitter.com/1.1/statuses/update.json',$parameters);
		if($response)return json_decode($response);
		return false;
	}
	
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/direct_messages/new
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function directMessagesNew($parameters){
		if(!isset($parameters['text']) || !(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/direct_messages/new.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/friendships/create
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function friendshipsCreate($parameters){
		if(!(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/friendships/create.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/friendships/destroy
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function friendshipsDestroy($parameters){
		if(!(isset($parameters['user_id']) or isset($parameters['screen_name']) ))return false;
		$response = $this->post('https://api.twitter.com/1.1/friendships/destroy.json',$parameters);
		if($response)return json_decode($response);
		return false;
	 }
	 
	/**
	 * https://dev.twitter.com/docs/api/1.1/post/account/update_profile
	 * 連想配列で上記URLのParametersをキーにして
	 */
	 function accountUpdateProfile($parameters){
 		if( isset($parameters['name']) || isset($parameters['url'])
 			|| isset($parameters['location']) || isset($parameters['description']) ){
			$response = $this->post('https://api.twitter.com/1.1/account/update_profile.json',$parameters);
			if($response)return json_decode($response);
		}
		return false;
	 }
	
	private function post($url,$parameters,&$header = ''){
		$oauth = new \OAuth($this->oauthParameters['consumer_key'],$this->oauthParameters['consumer_secret']);
		$oauth->setToken($this->oauthParameters['access_token'],$this->oauthParameters['access_token_secret']);
		$oauth->setRequestEngine(OAUTH_REQENGINE_STREAMS);
		try{
			if($oauth->fetch($url,$parameters,'POST')){
				$response = $oauth->getLastResponse();
				$header = $oauth->getLastResponseHeaders();
				return $response;
			}
		}catch(\Exception $e){
			self::log(LOG_ERR,$e->getMessage());
		}
		return false;
	}
	
	public function log($code,$message){
		if($message instanceof \Exception){
			echo '['.$this->name().$this->version().'] '.$message->getMessage().' '.$message->getFile().':'.$message->getLine();
			syslog($code, '['.$this->name().$this->version().'] '.$message->getMessage().' '.$message->getFile().':'.$message->getLine());
		}else{
			echo '['.$this->name().$this->version().'] '.$message;
			syslog($code, '['.$this->name().$this->version().'] '.$message);
		}
	}
	
}