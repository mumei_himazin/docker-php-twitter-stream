<?php
namespace stream_bot\libs;

class RunThread extends \Thread{
	private $plugin;
	private $type;
	private $json;
	
	public function __construct($plugin,$type,$json){
		$this->plugin = $plugin;
		$this->type = $type;
		$this->json = $json;
	}
	
	public function run(){
		$type = $this->type;
		$plugin = $this->plugin;
		$json = $this->json;
		try{
			$plugin->$type($json);
		}catch(Exception $e){
			echo $e->getMessage(),"\n";
		}
	}
}