<?php

$app_name = 'stream_bot';

$plugins_dir = '/plugins/';

$conf_file = '/setting.php';

if(file_exists($conf_file) && is_file($conf_file) ){
	$config = include $conf_file;
}else{
	$config =[
		'consumer_key' 			=> $argv[1],
		'consumer_secret'		=> $argv[2],
		'access_token'			=> $argv[3],
		'access_token_secret'	=> $argv[4]
	];
}