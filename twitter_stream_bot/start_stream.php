<?php

require_once __DIR__.'/setting.php';
require_once __DIR__.'/libs/autoLoader.php';


$oauth = new \OAuth($config['consumer_key'],$config['consumer_secret']);
$oauth->setToken($config['access_token'],$config['access_token_secret']);

$pluginLoader = new \stream_bot\libs\PluginLoader($plugins_dir);
$pluginLoader->load($config);
$callbacks = [
	'onRunable'		=> function(){
		return true;
	},
	'onConection'	=> function($response,$header){
		echo "onConection resonse:".$response['code'].' '.$response['message'],"\n";
		syslog(LOG_INFO,"onConection resonse:".$response['code'].' '.$response['message']);
	},
	'onDisconection'	=> function($response,$header){
		echo "onDisconection resonse:".$response['code'].' '.$response['message'],"\n";
		syslog(LOG_INFO, "onDisconection resonse:".$response['code'].' '.$response['message']);
	},
	'onError'		=> function($response,$header){
		echo "onError resonse:".$response['code'].' '.$response['message'],"\n";
		syslog(LOG_ERR, "onError resonse:".$response['code'].' '.$response['message']);
		foreach($header as $name => $value){
			echo "header:",$name," : ",$value,"\n";
			syslog(LOG_ERR, "onError header:".$name." : ".$value);
		}
	},
	'onShutdown'	=> function(){
		syslog(LOG_ERR, "onShutdown");
	},
	'onJson'		=> function($json) use ($pluginLoader){
		$type = "";
		if(isset($json->sender)){
			$type = 'onSender';
		}elseif( isset($json->text)){
			$type = 'onStatus';
		}elseif( isset($json->direct_message)){
			$type = 'onDirectMessage';
		}elseif( isset($json->delete)){
			$type = 'onDelete';
		}elseif( isset($json->limit)){
			$type = 'onLimit';
		}elseif( isset($json->warning)){
			$type = 'onWarning';
		}elseif( isset($json->scrub_geo)){
			$type = 'onScrubGeo';
		}elseif( isset($json->friends)){
			$type = 'onFriends';
		}elseif( isset($json->event)){
			switch($json->event){
				case 'favorite':
					$type = 'onFavorite';
					break;
				case 'unfavorite':
					$type = 'onUnfavorite';
					break;
				case 'follow':
					$type = 'onFollow';
					break;
				case 'unfollow':
					$type = 'onUnfollow';
					break;
				case 'user_update':
					$type = 'onUserUpdate';
					break;
				case 'block':
					$type = 'onBlock';
					break;
				case 'unblock':
					$type = 'onUnblock';
					break;
				case 'list_member_added':
					$type = 'onListMemberAdded';
					break;
				case 'list_member_removed':
					$type = 'onListMemberRemoved';
					break;
				case 'list_user_subscribed':
					$type = 'onListUserSubscribed';
					break;
				case 'list_user_unsubscribed':
					$type = 'onListUserUnsubscribed';
					break;
				case 'list_created':
					$type = 'onListCreated';
					break;
				case 'list_updated':
					$type = 'onListUpdated';
					break;
				case 'list_destroyed':
					$type = 'onListDestroyed';
					break;
			}
		}
		if($type !== ""){
			foreach($pluginLoader->getPlugins() as $plugin){
				try{
					$thread = new stream_bot\libs\RunThread($plugin,$type,$json);
					$thread->start();
				}catch(Exception $e){
					syslog(LOG_INFO, $e);
				}
			}
		}
		
	}
];

$stream = new \stream_bot\libs\TwitterStream($oauth,$callbacks);
if(isset($config['debug']) && $config['debug']===true){
	$stream->enableDebug();
}
$stream->start();