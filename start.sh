#!/bin/bash -e

if [ -f "/setting.php" ];then
	service rsyslog start
	exec $@
else
	if [ -z "$CONSUMER" ];then
		echo "-e CONSUMER=xxxxxxxxxxxxxxxxxxx required"
		exit -1
	fi
	if [ -z "$SECRET" ];then
		echo "-e SECRET=xxxxxxxxxxxxxxxxxxx required"
		exit -1
	fi
	if [ -z "$ACCESS_TOKEN" ];then
		echo "-e ACCESS_TOKEN=xxxxxxxxxxxxxxxxxxx required"
		exit -1
	fi
	if [ -z "$ACCESS_SECRET" ];then
		echo "-e ACCESS_SECRET=xxxxxxxxxxxxxxxxxxx required"
		exit -1
	fi
	service rsyslog start
	exec $@ "$CONSUMER" "$SECRET" "$ACCESS_TOKEN" "$ACCESS_SECRET"
fi


