#docker-php-twitter-stream

## installed

- ubuntu 14.04.1
- php5 thread safety

## Build
	docker build --rm -t <image_name> docker-php-twitter-stream


## Setup
	mkdir /var/twitter_stream /var/twitter_stream/log /var/twitter_stream/plugins
	cp docker-php-twitter-stream/sample/setting.php /var/twitter_stream/setting.php
	cp docker-php-twitter-stream/sample/plugins /var/twitter_stream/plugins
	#コンシューマキーなどの設定
	vim /var/twitter_stream/setting.php
	

## Usage
	#設定ファイルを使用する場合
	docker run -d -v /var/twitter_stream/plugins:/plugins -v /var/twitter_stream/setting.php:/setting.php <image_name>
	
	#環境変数としてコンシューマキーなどを設定する場合
	docker run -d -v /var/twitter_stream/plugins:/plugins -e CONSUMER=xxxxxxx -e SECRET=xxxx -e ACCESS_TOKEN=xxxxx -e ACCESS_SECRET=xxxxx <image_name>
	
	#ログファイル
	-v /var/twitter_stream/log:/var/log オプションを追加してくだい

##sample
	docker build --rm -t twitter_stream docker-php-twitter-stream
	
	mkdir /var/twitter_stream /var/twitter_stream/log /var/twitter_stream/plugins
	cp docker-php-twitter-stream/sample/setting.php /var/twitter_stream/setting.php
	cp docker-php-twitter-stream/sample/plugins /var/twitter_stream/plugins
	#コンシューマキーなどの設定
	vim /var/twitter_stream/setting.php
	
	docker run -d -v /var/twitter_stream/plugins:/plugins -v /var/twitter_stream/setting.php:/setting.php -v /var/twitter_stream/log:/var/log twitter_stream
