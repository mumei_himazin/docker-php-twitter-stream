FROM ubuntu:14.04.1

RUN echo "deb http://jp.archive.ubuntu.com/ubuntu/ trusty main restricted\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty main restricted\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty-updates main restricted\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty-updates main restricted\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty universe\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty universe\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty-updates universe\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty-updates universe\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty multiverse\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty multiverse\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty-updates multiverse\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty-updates multiverse\n\
	deb http://jp.archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse\n\
	deb-src http://jp.archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse\n\
	deb http://security.ubuntu.com/ubuntu trusty-security main restricted\n\
	deb-src http://security.ubuntu.com/ubuntu trusty-security main restricted\n\
	deb http://security.ubuntu.com/ubuntu trusty-security universe\n\
	deb-src http://security.ubuntu.com/ubuntu trusty-security universe\n\
	deb http://security.ubuntu.com/ubuntu trusty-security multiverse\n\
	deb-src http://security.ubuntu.com/ubuntu trusty-security multiverse\n"> /etc/apt/sources.list &&\
	apt-get update && apt-get -y upgrade && apt-get -y clean
	
RUN apt-get -y install php5-common php5-dev php5-gd snmp snmpd
	
ADD build_php /build_php

RUN cd /build_php && dpkg -i *.deb

RUN apt-get -y install libpcre3 libpcre3-dev \
	&& pecl install pthreads \
	&& echo "extension=pthreads.so" > /etc/php5/mods-available/pthreads.ini \
	&& ln -s /etc/php5/mods-available/pthreads.ini /etc/php5/cli/conf.d/20-pthreads.ini
	
RUN pecl install oauth \
	&& echo "extension=oauth.so" > /etc/php5/mods-available/oauth.ini \
	&& ln -s /etc/php5/mods-available/oauth.ini /etc/php5/cli/conf.d/20-oauth.ini
	
RUN sed -i -e "s/;error_log = syslog/error_log = syslog/" /etc/php5/cli/php.ini
ADD 40-php.conf /etc/rsyslog.d/40-php.conf
	
ADD twitter_stream_bot /stream
ADD start.sh /start.sh
RUN chmod 700 /start.sh

RUN php -v

VOLUME /plugins

ENTRYPOINT ["/start.sh"]
CMD	["php","/stream/start_stream.php"]

